using MutableTuples
@static if VERSION < v"0.7.0-DEV.2005"
    using Base.Test
else
    using Test
end

function check_getindex(x::MutableTuple{T}, data::U) where {T, U}
    for i in 1:length(x)
        @eval @test (@inferred $x[Val{$i}])::$(T.parameters[i]) == $(data[i])
    end
end

@testset "constructors, getindex, setindex!" begin
    data = (1, 2.0, true)
    x = (@inferred MutableTuple(data))::MutableTuple{typeof(data)}
    check_getindex(x, data)

    # setindex!
    x[Val{1}] = 10
    x[Val{2}] = 100
    x[Val{3}] = false
    check_getindex(x, (10, 100, false))

    # errors
    @test_throws InexactError x[Val{3}] = 2
    @test_throws ArgumentError MutableTuple((1, "abc"))
end

@testset "basic functions" begin
    x = MutableTuple((1, 2, 3))
    @test (@inferred length(x))::Int == 3
    @test (@inferred eachindex(x))::UnitRange{Int} == 1:3
    @test (@inferred Tuple(x))::Tuple{Int,Int,Int} === x.data
    @test @mtuple(1, 2, 3) == @mtuple(1, 2, 3)
end

@testset "macros" begin
    data = (1, 2.0, true)
    @test @mtuple(1, 2.0, true) == @mtuple(data)
    x = @mtuple data

    # getindex
    @test @mtuple(x[1]) == 1
    @test @mtuple(x[2]) == 2.0
    @test @mtuple(x[3]) == true

    # setindex!
    @mtuple x[1] = 10
    @mtuple x[2] = 100
    @mtuple x[3] = false
    check_getindex(x, (10, 100, false))

    # errors
    @test_throws MethodError @mtuple 3
end
