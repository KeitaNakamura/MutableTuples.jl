__precompile__()

module MutableTuples

using Base: unsafe_convert

export MutableTuple, @mtuple

mutable struct MutableTuple{T <: Tuple}
    data::T
    function MutableTuple{T}(data::T) where T
        isbits(data) || throw(ArgumentError("MutableTuple with non-isbits eltype is not supported."))
        new(data)
    end
end
@inline MutableTuple(x::T) where {T <: Tuple} = MutableTuple{T}(x)

@inline Base.length(x::MutableTuple) = length(x.data)
@inline Base.eachindex(x::MutableTuple) = 1:length(x)

@inline Base.Tuple(x::MutableTuple) = x.data

@inline function Base.getindex(x::MutableTuple, ::Type{Val{I}}) where {I}
    x.data[I]
end

@inline function Base.setindex!(x::MutableTuple, v, i::Type{<: Val})
    _unsafe_store!(x, v, i)
end

@generated function _unsafe_store!(x::MutableTuple{T}, v, ::Type{Val{I}}) where {T, I}
    types = T.parameters
    if I < 1 || I > length(types)
        throw(BoundsError(x, I))
    end
    offset = 0
    for t in types[1:I-1]
        offset += sizeof(t)
    end
    return quote
        x_ptr = unsafe_convert(Ptr{Void}, Ref(x))
        v_ptr = unsafe_convert(Ptr{Void}, Ref(convert($(types[I]), v)))
        dst = unsafe_convert(Ptr{UInt8}, x_ptr)
        src = unsafe_convert(Ptr{UInt8}, v_ptr)
        for i in 1:sizeof($(types[I]))
            unsafe_store!(dst, unsafe_load(src, i), $offset + i)
        end
        return v
    end
end

@inline Base.:(==)(x::MutableTuple, y::MutableTuple) = Tuple(x) == Tuple(y)

macro mtuple(args...)
    esc(:($MutableTuple(tuple($(args...)))))
end

macro mtuple(expr)
    if Meta.isexpr(expr, :ref)
        return esc(getindex_expr(expr))
    elseif Meta.isexpr(expr, :(=))
        lhs = expr.args[1]
        rhs = expr.args[2]
        Meta.isexpr(lhs, :ref) && return esc(:($(getindex_expr(lhs)) = $rhs))
    else
        return esc(:($MutableTuple($expr)))
    end
    throw(ArgumentError("@mtuple: invalid expression `$expr`."))
end

# `expr` must be `Meta.isexpr(expr, :ref) == true`
@inline function getindex_expr(expr::Expr)
    :($(expr.args[1])[$(Val{expr.args[2]})])
end

end # module
